#include "ClsGame.h"
#include <iostream>

int clsGame::CLSGAME()
{
	// CREAR VENTANA
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
	window.setVerticalSyncEnabled(true);
	
	// CARGAR SPRITES
	//Fondo
	sf::Texture texture_back;
	if (!texture_back.loadFromFile("skyline.png"))
		return EXIT_FAILURE;
	sf::Sprite Background(texture_back);
	//Nave
	sf::Texture texture;
	if (!texture.loadFromFile("nave1.png"))
		return EXIT_FAILURE;
	sf::Sprite nave(texture);
	nave.setPosition(window.getSize().x/2, window.getSize().y/2);
	//Enemigo
	std::vector<sf::Sprite> v;

	
	sf::Texture texture_enemigo;
	if (!texture_enemigo.loadFromFile("enemigo.png"))
		return EXIT_FAILURE;
	sf::Sprite enemigo(texture_enemigo);
	enemigo.setPosition(200,200);
	enemigo.setScale(sf::Vector2f(0.5, 0.5));
	enemigo.setRotation(180);
	

	// TEXTO
	sf::Font font;
	if (!font.loadFromFile("Minecraft.ttf"))
		return EXIT_FAILURE;
	sf::Text text("Score:", font, 30);


	// LOOP
	while (window.isOpen())
	{
		//SCORE
		score += 1;
		
		// EVENTOS
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed)
				window.close();
		}

		//MOVIMIENTO nave
		sf::Vector2f movBala(0.f, 0.f);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			UP = true;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			DOWN = true;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			LEFT = true;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			RIGHT = true;

		
		sf::Vector2f movimiento(0.f,0.f);
		if (UP) { movimiento.y -= 1.f*playerMov; UP = false; }
		if (DOWN) { movimiento.y += 1.f*playerMov; DOWN = false; }
		if (LEFT) { movimiento.x -= 1.f*playerMov; LEFT = false; }
		if (RIGHT) { movimiento.x += 1.f*playerMov; RIGHT = false; }


		nave.move(movimiento);
		//FIN MOVIMIENTO nave
		if (nave.getGlobalBounds().intersects(enemigo.getGlobalBounds()))
			enemigo.setColor(sf::Color::Cyan);
		// Clear screen
		window.clear();
		// Draw the sprite
		window.draw(Background);
		window.draw(enemigo);
		window.draw(nave);
		// Draw the string
		window.draw(text);
		// Update the window
		window.display();
	}
}
